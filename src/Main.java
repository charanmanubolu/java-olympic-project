import java.security.Key;
import java.util.*;

public class Main {
    static void YearWiseNumberOfGoldMedalsWonByEachPlayer(List<Map<String, String>> data) {
        try {
            Map<Integer, Map<String, Integer>> result = new HashMap<>();
            for (int index = 0; index < data.size(); index++) {
                Map<String, String> entry = data.get(index);
                if (entry.get("Medal").equalsIgnoreCase("Gold")) {
                    int year = Integer.parseInt(entry.get("Year"));
                    String name = entry.get("Name");


                    if (!result.containsKey(year)) {
                        result.put(year, new HashMap<>());
                    }

                    Map<String, Integer> athleteData = result.get(year);
                    athleteData.put(name, athleteData.getOrDefault(name, 0) + 1);
                }
            }
            System.out.println(result);
        } catch (Exception e) {
            System.err.println("Unexpected error in entry in first problem: " + e.getMessage());
        }
    }

    static void AthletesWhoWonGoldMedalIn1980AndAgeIsLessThan30Years(List<Map<String, String>> data) {
        List<String> result = new LinkedList<>();
        try {
            for (int Index = 0; Index < data.size(); Index++) {
                Map<String, String> entry = data.get(Index);
                if (entry.get("Age").equals("NA")) {
                    continue;
                }
                if (entry.get("Medal").equalsIgnoreCase("Gold") && Integer.parseInt(entry.get("Age")) < 30 && Integer.parseInt(entry.get("Year")) == 1980) {
                    result.add(entry.get("Name"));
                }
            }
        } catch (Exception e) {
            System.out.println("Unexpected error in entry in second problem: " + e);
        } finally {
            System.out.println(result);
        }
    }

    static void eventWiseNumberOfGoldSilverBronzeMedalsInYear1980(List<Map<String, String>> data) {
        try {
            Map<String, Map<String, Integer>> result = new HashMap<>();
            for (Map<String, String> entry : data) {
                String Event = entry.get("Event");
                String Medal = entry.get("Medal");
                if (!Medal.equals("NA") && entry.get("Year").equals("1980")) {
                    if (!result.containsKey(Event)) {
                        result.put(Event, new HashMap<>());
                    }
                        Map<String, Integer> MedalsData = result.get(Event);
                        MedalsData.put(Medal, MedalsData.getOrDefault(Medal, 0) + 1);

                }
            }

            System.out.println(result);
        } catch (Exception e) {
            System.out.println("Unexpected error in entry in third problem: " + e);
        }
    }

    static void displayGoldWinnerOfFootballOfEveryOlympic(List<Map<String, String>> data) {
        Map<Integer, ArrayList<String>> result = new HashMap<>();
        try {
            for (Map<String, String> entry : data) {
                String sport = entry.get("Sport");
                String medal = entry.get("Medal");
                int year = Integer.parseInt(entry.get("Year"));
                if (sport.equalsIgnoreCase("Football") && medal.equalsIgnoreCase("Gold")) {
                    if (!result.containsKey(year)) {
                        result.put(year, new ArrayList<>());
                    }
                    ArrayList<String> Winner = result.get(year);
                    Winner.add(entry.get("Name"));
                }
            }
            System.out.println(result);
        } catch (Exception e) {
            System.out.println("Unexpected error in entry in fourth problem: " + e.getMessage());
        }
    }

    static void femaleAthleteWonMaximumNumberOfGoldinAllOlympics(List<Map<String, String>> data) {
        Map<String, Integer> result = new HashMap<>();
        try {
            for (Map<String, String> entry : data) {
                String sex = entry.get("Sex");
                String medal = entry.get("Medal");
                String name = entry.get("Name");
                if (sex.equalsIgnoreCase("F") && medal.equalsIgnoreCase("Gold")) {
                    if (!result.containsKey(name)) {
                        result.put(name, result.getOrDefault(name, 0));
                    }
                    result.put(name, result.getOrDefault(name, 0) + 1);
                }
            }
            List<Map.Entry<String, Integer>> entries = new ArrayList<>(result.entrySet());
            entries.sort(Map.Entry.<String, Integer>comparingByValue().reversed());

            Map<String, Integer> sortedMap = new LinkedHashMap<>();
            for (Map.Entry<String, Integer> entry : entries) {
                sortedMap.put(entry.getKey(), entry.getValue());
            }
            System.out.println(entries.get(0));
        } catch (Exception e) {
            System.out.println("Unexpected error in entry in fifth problem: " + e.getMessage());
        }
    }

    static void nameOfAthletewhoParticipatedInMoreThanThreeOlympics(List<Map<String, String>> data) {
        Map<String,Integer> result = new HashMap<>();
        try{
            for(Map<String,String> entry : data){
                if(!result.containsKey(entry.get("Name"))){
                    result.put(entry.get("Name"),0);
                }
                result.put(entry.get("Name"),result.get(entry.get("Name"))+1);
            }
           List<Map.Entry<String,Integer>> Items = new ArrayList<>(result.entrySet());
            Map <String,Integer> FinalResult = new HashMap<>();
            for(Map.Entry<String,Integer> entry : Items){
//                System.out.println(entry.getValue());
                if(entry.getValue() >= 3){
                    FinalResult.put(entry.getKey(),entry.getValue());
                }
            }
            System.out.println(FinalResult);

        }catch (Exception e){
            System.out.println("Unexpected error in entry in sixth problem: " + e.getMessage());
        }
    }

    static  void  displayTheYearWiseCountryThatWonTheHighestMedals(List<Map<String,String>> data){
        Map<Integer,Map<String,Integer>> resultoFYearWiseCountrysThatWonMedals = new HashMap<>();
        try{
            for(Map<String,String> entry:data){
                String team = entry.get("Team");
                String medal = entry.get("Medal");
                int year = Integer.parseInt(entry.get("Year"));

                if(!medal.equalsIgnoreCase("NA")){
                    if(!resultoFYearWiseCountrysThatWonMedals.containsKey(year)){
                        resultoFYearWiseCountrysThatWonMedals.put(year , new HashMap<>());
                    }
                    Map<String,Integer> teamAndMedals = resultoFYearWiseCountrysThatWonMedals.get(year);
                    teamAndMedals.put(team,teamAndMedals.getOrDefault(team,0)+1);
                }
            }
            for(int year : resultoFYearWiseCountrysThatWonMedals.keySet()){
                Map<String,Integer> yearData = resultoFYearWiseCountrysThatWonMedals.get(year);
                String countryName = "";
                int value = 0;
                for(Map.Entry<String,Integer> entry : yearData.entrySet()){
                    if(entry.getValue() > value){
                        value= entry.getValue();
                        countryName = entry.getKey();
                    }
                }
                System.out.println(year +":"+countryName +" won "+value +" times");
            }
//            System.out.println(resultoFYearWiseCountrysThatWonMedals);
        }catch (Exception e){
            System.out.println("Unexpected error in entry in seventh problem: " + e.getMessage());
        }
    }

    public static void main(String[] args) {
        csvToMap obj = new csvToMap();
        List<Map<String, String>> data = obj.Converter("athlete_events");
        YearWiseNumberOfGoldMedalsWonByEachPlayer(data);
        AthletesWhoWonGoldMedalIn1980AndAgeIsLessThan30Years(data);
        eventWiseNumberOfGoldSilverBronzeMedalsInYear1980(data);
        displayGoldWinnerOfFootballOfEveryOlympic(data);
        femaleAthleteWonMaximumNumberOfGoldinAllOlympics(data);
        nameOfAthletewhoParticipatedInMoreThanThreeOlympics(data);
        displayTheYearWiseCountryThatWonTheHighestMedals(data);
    }
}